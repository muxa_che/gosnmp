
package main

import (
	"./gosnmp"
	"os"
)

func main() {
	ip := os.Args[1]
	community := os.Args[2]
	oid := os.Args[3]
	gosnmp.Work(ip, community, oid)
}