package gosnmp

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"runtime"
	"time"
)

type OriginData struct {
	Address    string
	Durarion   string
	Iterations int
}

type Conn struct {
	IPAddr string
	buf    [512]byte
}

type Timing struct {
	Succeful      int64
	AttemtsAmount int64
}

func Work(ip, comm, oidTree string) {
	var maxTimeoutUse int64
	maxTimeoutUse = 10
	runtime.GOMAXPROCS(runtime.NumCPU())
	s := new(SnmpPDU)
	s.community = []byte(comm)
	c := new(Conn)
	c.IPAddr = ip
	oid := OidToByte(oidTree)
	t := new(Timing)
	for {
		t0 := time.Now()
		var vnn int
		dub := oid
		for {
			pdu := s.SnmpPduBuilder(oid, 161)
			res, maxTimeout, attempt := c.Transfer(pdu, maxTimeoutUse)
			maxTimeoutUse = t.Timeout(maxTimeout, attempt, vnn)
			oidNext, oidType, dataString, _ := NextOid(res)
			oid = oidNext

			if string(dub) != string(oid[0:len(dub)]) {
				t1 := time.Now()
				diff := t1.Sub(t0)
				fmt.Printf("Duration: %s\n", diff)
				TransToRemoteLog(diff.String(), c.IPAddr, vnn)
				os.Exit(0)
			}
			//fmt.Printf("Iteration # %d  ", vnn)
			//fmt.Printf("Attempts: %d ", attempt)
			//fmt.Printf("Timeout: %d ", maxTimeout)
			fmt.Printf("%s ", ByteToOid(oid))
			fmt.Printf("%s: ", oidType)
			//fmt.Printf("TYPE: %v: ", rType)
			fmt.Printf("%v\n", dataString)
			vnn++
		}
	}
}

func (c Conn) Transfer(PDU []byte, maxTimeout int64) ([512]byte, int64, int) {
	service := c.IPAddr + ":161"
	c.buf[0] = 0
	var attempt int
	for i := 1; i <= 500; i++ {
		attempt = i
		udpAddr, err := net.ResolveUDPAddr("udp", service)
		checkError(err)
		conn, err := net.DialUDP("udp", nil, udpAddr)
		//conn, err := net.DialTimeout("udp", service, 1*time.Second)
		checkError(err)
		conn.Write(PDU)
		//t0 := time.Now()
		go conn.ReadFromUDP(c.buf[0:])

		if c.buf[0] == 0 {
			var n int64
			for n = 0; n < maxTimeout; n++ {
				time.Sleep(1 * time.Millisecond)
				if c.buf[0] == 0 {
					continue
				} else {
					// This part will detele.
					//if i == 1 && maxTimeout > 30 {
					//	maxTimeout -= 10
					//}
					//
					conn.Close()
					break
				}
			}
		} else {
			conn.Close()
			break
		}
		if c.buf[0] == 0 {
			maxTimeout += 10
			conn.Close()
		} else {
			conn.Close()
			break
		}
	}
	return c.buf, maxTimeout, attempt
}

func (t *Timing) Timeout(maxTimeout int64, attempt int, iterations int) int64 {
	maxNewTimeout := maxTimeout
	t.AttemtsAmount += int64(attempt)
	if attempt == 1 {
		t.Succeful += 1
	} else {
		t.Succeful = 0
	}
	if iterations > 5 && t.AttemtsAmount/int64(iterations) < 2 && t.Succeful > 10 {
		if maxTimeout > 30 {
			maxNewTimeout -= 10
		}
	}
	//fmt.Println(maxNewTimeout)
	return maxNewTimeout
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}

func TransToRemoteLog(diff string, ip string, iterations int) {
	originData := new(OriginData)
	originData.Address = ip
	originData.Durarion = diff
	originData.Iterations = iterations
	service := "noc.ti.ru:1288"

	conn, err1 := net.Dial("tcp", service)
	if err1 != nil {
		os.Exit(0)
	}

	encoder := json.NewEncoder(conn)
	encoder.Encode(originData)
	conn.Close()
}
