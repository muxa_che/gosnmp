package gosnmp


type SnmpPDU struct {
	oid             []byte
	oidHeader       []byte
	dataHeader      []byte
	community       []byte
	communityHeader []byte
	pdu             []byte
}

func (s *SnmpPDU) SnmpPduBuilder(oid []byte, respType byte) []byte {
	s.SnmpDataHeader(respType, oid)
	s.SnmpCommunityHeader()
	var pdu []byte
	value := []byte{2, 1, 1}
	pdu = ByteArrayAppend(s.communityHeader, s.dataHeader)
	pdu = ByteArrayAppend(value, pdu)
	snmpPduLen := []byte{48, byte(len(pdu))}
	pdu = ByteArrayAppend(snmpPduLen, pdu)
	return pdu
}

func (s *SnmpPDU) SnmpOidHeader(oid []byte) {
	value := []byte{5, 0}
	oidLen := []byte{6, byte(len(oid))}
	oidValue := ByteArrayAppend(oid, value)
	oidLenOid := ByteArrayAppend(oidLen, oidValue)
	oidHeaderLen := []byte{48, byte(len(oidLenOid))}
	s.oidHeader = ByteArrayAppend(oidHeaderLen, oidLenOid)
}

func (s *SnmpPDU) SnmpDataHeader(respType byte, oid []byte) {
	s.SnmpOidHeader(oid)
	dataEndHeaderLen := []byte{48, byte(len(s.oidHeader))}
	dataEndOidHeader := ByteArrayAppend(dataEndHeaderLen, s.oidHeader)
	errorHeader := []byte{2, 1, 0, 2, 1, 0}
	errorHeaderLen := ByteArrayAppend(errorHeader, dataEndOidHeader)
	requestId := []byte{72, 150, 55, 242}
	requestErrorLen := ByteArrayAppend(requestId, errorHeaderLen)
	dataPre := []byte{2, 4}
	dataRequestErrorHeader := ByteArrayAppend(dataPre, requestErrorLen)
	dataHeaderLen := []byte{respType, byte(len(dataRequestErrorHeader))}
	s.dataHeader = ByteArrayAppend(dataHeaderLen, dataRequestErrorHeader)
}

func (s *SnmpPDU) SnmpCommunityHeader() {
	communityLen := []byte{4, byte(len(s.community))}
	s.communityHeader = ByteArrayAppend(communityLen, s.community)
}

func ByteArrayAppend(data, newdata []byte) []byte {
	for i := 0; i < len(newdata); i++ {
		data = append(data, newdata[i])
	}
	return data
}