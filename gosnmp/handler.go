package gosnmp

func NextOid(buf [512]byte) ([]byte, string, []byte, []byte) {
	var oidLen int
	var datalen int
	var data []byte
	if buf[2] == 2 {
		oidLen = int(buf[6]) + 6 + 20

	} else if buf[2] == 0 {
		oidLen = int(buf[8]) + 6 + 28

	}
	nOid := buf[oidLen+1 : oidLen+int(buf[oidLen])+1]
	rType := buf[oidLen+int(buf[oidLen])+1 : oidLen+int(buf[oidLen])+3]

	if rType[1] > 128 {
		rType = buf[oidLen+int(buf[oidLen])+1 : oidLen+int(buf[oidLen])+5]
		datalen = int(rType[1])/128 + int(rType[3]) + 1
		data = buf[oidLen+int(buf[oidLen])+5 : oidLen+int(buf[oidLen])+3+datalen]
	} else {
		datalen = int(rType[1])
		data = buf[oidLen+int(buf[oidLen])+3 : oidLen+int(buf[oidLen])+3+datalen]
	}

	resType := TypeToString(rType)
	return nOid, resType, data, rType
}

func TypeToString(tString []byte) string {
	var oidType string
	switch tString[0] {

	case 4:
		oidType = "STRING"

	case 2:
		oidType = "INTEGER"

	case 6:
		oidType = "OID"

	case 64:
		oidType = "IpAddress"

	case 65:
		oidType = "Counter32"

	case 66:
		oidType = "Gauge32"

	case 67:
		oidType = "Timeticks"
	}

	return oidType
}