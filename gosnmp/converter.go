package gosnmp

import (
	"strconv"
	"strings"
)

func ByteToOid(oidBytes []byte) string {
	var result string
	var fByte int
	var sByte int
	var sByteP int
	fByte = int(oidBytes[0])
	result += strconv.Itoa(fByte / 40)
	result += "."
	result += strconv.Itoa(fByte % 40)
	result += "."
	for i := 1; i < len(oidBytes); i++ {
		sByte = int(oidBytes[i])
		if sByte > 128 {
			sByteP = int(oidBytes[i+1])
			result += strconv.Itoa(((sByte - 128) * 128) + sByteP)
			i++
		} else {
			result += strconv.Itoa(sByte)
		}
		if i == len(oidBytes)-1 {
			break
		} else {
			result += "."
		}
	}
	return result
}

func OidToByte(oid string) []byte {
	var firstTwoNum1 int
	var firstTwoNum2 int
	var OIdBytes []byte
	var sByte int
	oidArray := strings.Split(oid, ".")
	if oidArray[0] == "" {
		firstTwoNum1, _ = strconv.Atoi(oidArray[1])
		firstTwoNum2, _ = strconv.Atoi(oidArray[2])
		OIdBytes = append(OIdBytes, byte(firstTwoNum1*40+firstTwoNum2))
		for i := 3; i < len(oidArray); i++ {
			sByte, _ = strconv.Atoi(oidArray[i])
			if sByte > 128 {
				OIdBytes = append(OIdBytes, byte(sByte/128+128))
				OIdBytes = append(OIdBytes, byte(sByte-((sByte/128)*128)))
			} else {
				OIdBytes = append(OIdBytes, byte(sByte))
			}

		}
	} else if oidArray[0] == "1" {
		firstTwoNum1, _ = strconv.Atoi(oidArray[0])
		firstTwoNum2, _ = strconv.Atoi(oidArray[1])
		OIdBytes = append(OIdBytes, byte(firstTwoNum1*40+firstTwoNum2))
		for i := 2; i < len(oidArray); i++ {
			sByte, _ = strconv.Atoi(oidArray[i])
			if sByte > 128 {
				OIdBytes = append(OIdBytes, byte(sByte/128+128))
				OIdBytes = append(OIdBytes, byte(sByte-((sByte/128)*128)))
			} else {
				//sByte, _ = strconv.Atoi(oidArray[i])
				OIdBytes = append(OIdBytes, byte(sByte))
			}
		}
	}
	return OIdBytes
}